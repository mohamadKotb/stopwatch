package harmoush.com.stopwatch.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import harmoush.com.stopwatch.R;
import harmoush.com.stopwatch.models.Time;
import harmoush.com.stopwatch.models.StopwatchLap;

public class LapsAdapter extends RecyclerView.Adapter<LapsAdapter.LapsViewHolder> {

    private ArrayList<StopwatchLap> laps;
    private Context mContext;

    public LapsAdapter(ArrayList<StopwatchLap> laps) {
        this.laps = laps;
    }

    @NonNull
    @Override
    public LapsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.cell_lap, viewGroup, false);
        return new LapsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LapsViewHolder lapsViewHolder, int position) {
        StopwatchLap lap = laps.get(position);

        lapsViewHolder.lapIdTextView.setText(String.format("#%d", position + 1));
        Time lapStartTime = lap.getStartTime();
        Time lapEndTime = lap.getEndTime();

        lapsViewHolder.lapStartTimeTextView.setText(String.format("%02d:%02d", lapStartTime.getMinutes(), lapStartTime.getSeconds()));
        lapsViewHolder.lapEndTimeTextView.setText(String.format("%02d:%02d", lapEndTime.getMinutes(), lapEndTime.getSeconds()));
    }

    @Override
    public int getItemCount() {
        return laps.size();
    }

    class LapsViewHolder extends RecyclerView.ViewHolder {
        private TextView lapIdTextView;
        private TextView lapStartTimeTextView;
        private TextView lapEndTimeTextView;

        LapsViewHolder(@NonNull View itemView) {
            super(itemView);
            lapIdTextView = itemView.findViewById(R.id.tv_lap_id);
            lapStartTimeTextView = itemView.findViewById(R.id.tv_lap_start_time);
            lapEndTimeTextView = itemView.findViewById(R.id.tv_lap_end_time);
        }
    }
}
