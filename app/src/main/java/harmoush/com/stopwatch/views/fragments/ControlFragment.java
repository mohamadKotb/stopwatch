package harmoush.com.stopwatch.views.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import harmoush.com.stopwatch.R;
import harmoush.com.stopwatch.viewmodels.StopwatchViewModel;

public class ControlFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = ControlFragment.class.getSimpleName();

    private StopwatchViewModel stopwatchViewModel;
    private boolean stopwatchPaused = true;
    private String currentTime;
    private TextView stopwatchTimeTextView;
    private ImageButton startPauseImageButton;
    private Button mResetButton;
    private Button mLapButton;

    @SuppressLint("DefaultLocale")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        stopwatchViewModel = ViewModelProviders.of(getActivity()).get(StopwatchViewModel.class);
        stopwatchViewModel.getElapsedTime().observe(this, lapTime -> {

            if (lapTime != null) {
                currentTime = String.format("%02d:%02d", lapTime.getMinutes(), lapTime.getSeconds());
                stopwatchTimeTextView.setText(currentTime);
            }
        });

        stopwatchViewModel.getStopwatchPaused().observe(this, stopwatchPaused -> {

            this.stopwatchPaused = stopwatchPaused != null ? stopwatchPaused : true;
            updateUi();
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_control, container, false);

        initUi(fragment);
        updateUi();
        return fragment;
    }

    private void initUi(View fragment) {

        stopwatchTimeTextView = fragment.findViewById(R.id.tv_stopwatch_time);
        startPauseImageButton = fragment.findViewById(R.id.btn_start_pause);
        mLapButton = fragment.findViewById(R.id.btn_lap);
        mResetButton = fragment.findViewById(R.id.btn_reset);

        startPauseImageButton.setOnClickListener(this);
        mLapButton.setOnClickListener(this);
        mResetButton.setOnClickListener(this);

    }

    private void updateUi() {

        if (stopwatchPaused) {
            startPauseImageButton.setImageResource(R.drawable.icon_play);
            mResetButton.setVisibility(View.VISIBLE);
            mLapButton.setVisibility(View.INVISIBLE);

        } else {
            startPauseImageButton.setImageResource(R.drawable.icon_pause);
            mLapButton.setVisibility(View.VISIBLE);
            mResetButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        stopwatchTimeTextView.setText(currentTime);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_start_pause:

                startPauseStopwatch();
                break;
            case R.id.btn_lap:

                addNewStopwatchLap();
                break;
            case R.id.btn_reset:

                resetStopwatch();
                break;
        }
    }

    private void addNewStopwatchLap() {
        stopwatchViewModel.addStopwatchLap();
        showToast(R.string.new_lap_added);
    }

    private void showToast(int messageId) {
        Toast.makeText(getContext(), messageId, Toast.LENGTH_SHORT).show();
    }


    private void resetStopwatch() {

        stopwatchViewModel.resetLaps();
        showToast(R.string.stopwatch_reset);
    }

    private void startPauseStopwatch() {

        if (stopwatchPaused) {
            stopwatchViewModel.startStopwatch();
        } else {
            stopwatchViewModel.pauseStopwatch();
        }
    }
}
