package harmoush.com.stopwatch.views.activities;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.navigation.Navigation;
import harmoush.com.stopwatch.R;
import harmoush.com.stopwatch.viewmodels.StopwatchViewModel;

public class MainActivity extends AppCompatActivity {

    private Button goToLapsListButton;
    private MutableLiveData<Boolean> showBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUi();
    }

    private void initUi() {

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

            goToLapsListButton = findViewById(R.id.btn_go_to_laps);
            goToLapsListButton.setOnClickListener(v -> goToLapsView());

            StopwatchViewModel stopwatchViewModel = ViewModelProviders.of(this).get(StopwatchViewModel.class);
            showBackButton = stopwatchViewModel.getShowBackButton();
            showBackButton.observe(this, show -> updateUi());
        }
    }

    private void goToLapsView() {

        showBackButton.setValue(true);
        Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.action_controlFragment_to_lapsListFragment);
    }

    private void updateUi() {

        String title;
        if (showBackButton.getValue()) {
            title = getString(R.string.laps);
        } else {
            title = getString(R.string.home);
        }
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(showBackButton.getValue());

        goToLapsListButton.setVisibility(showBackButton.getValue() ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        showBackButton.setValue(false);
    }
}
