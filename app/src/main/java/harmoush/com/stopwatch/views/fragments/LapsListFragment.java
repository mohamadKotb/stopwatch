package harmoush.com.stopwatch.views.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import harmoush.com.stopwatch.R;
import harmoush.com.stopwatch.models.StopwatchLap;
import harmoush.com.stopwatch.viewmodels.StopwatchViewModel;
import harmoush.com.stopwatch.views.adapters.LapsAdapter;

public class LapsListFragment extends Fragment {

    private ArrayList<StopwatchLap> laps;
    private LapsAdapter lapsAdapter;
    private RecyclerView mLapsListRecyclerView;

    private StopwatchViewModel stopwatchViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        laps = new ArrayList<>();
        stopwatchViewModel = ViewModelProviders.of(getActivity()).get(StopwatchViewModel.class);

        stopwatchViewModel.getElapsedTime().observe(this, lapTime -> {
            if (laps != null && laps.size() > 0) {

                StopwatchLap lastLap = laps.get(laps.size() - 1);
                lastLap.setEndTime(lapTime);
                laps.set(laps.size() - 1, lastLap);
                lapsAdapter.notifyDataSetChanged();
                scrollToLastLap();
            }
        });
        stopwatchViewModel.getStopwatchLaps().observe(this, stopwatchLaps -> {
            laps.clear();
            laps.addAll(stopwatchLaps);
            lapsAdapter.notifyDataSetChanged();
            scrollToLastLap();
        });
    }

    private void scrollToLastLap() {
        mLapsListRecyclerView.scrollToPosition(laps.size());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_laps_list, container, false);

        initUi(fragment);
        return fragment;
    }

    private void initUi(View fragment) {

        lapsAdapter = new LapsAdapter(laps);
        mLapsListRecyclerView = fragment.findViewById(R.id.rv_laps);

        mLapsListRecyclerView.setAdapter(lapsAdapter);
        mLapsListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true));
    }
}
