package harmoush.com.stopwatch.models;

public class StopwatchLap {

    private Time startTime;
    private Time endTime;

    public StopwatchLap() {
        startTime = new Time();
        endTime = new Time();
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = new Time(startTime);
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = new Time(endTime);
    }
}
