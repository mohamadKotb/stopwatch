package harmoush.com.stopwatch.models;

public class Time {

    private int minutes;
    private int seconds;

    public Time() {
        minutes = 0;
        seconds = 0;
    }

    public Time(Time time) {
        minutes = time.getMinutes();
        seconds = time.getSeconds();
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}
