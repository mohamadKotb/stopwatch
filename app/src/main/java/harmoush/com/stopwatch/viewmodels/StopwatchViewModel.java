package harmoush.com.stopwatch.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;

import harmoush.com.stopwatch.managers.StopwatchManager;
import harmoush.com.stopwatch.managers.StopwatchManager.ElapsedTimeUpdatedListener;
import harmoush.com.stopwatch.models.Time;
import harmoush.com.stopwatch.models.StopwatchLap;

public class StopwatchViewModel extends ViewModel {

    private MutableLiveData<ArrayList<StopwatchLap>> stopwatchLaps = new MutableLiveData<>();

    private MutableLiveData<Boolean> stopwatchPaused = new MutableLiveData<>();

    private MutableLiveData<Time> elapsedTime = new MutableLiveData<>();
    private StopwatchManager stopwatchManager;

    private MutableLiveData<Boolean> showBackButton = new MutableLiveData<>();
    public StopwatchViewModel() {

        elapsedTime.setValue(new Time());
        stopwatchLaps.setValue(new ArrayList<>());

        showBackButton.setValue(false);

        ElapsedTimeUpdatedListener elapsedTimeUpdatedListener = time -> elapsedTime.postValue(time);
        stopwatchManager = new StopwatchManager(elapsedTimeUpdatedListener);
    }

    public MutableLiveData<Time> getElapsedTime() {
        return elapsedTime;
    }

    public MutableLiveData<Boolean> getShowBackButton() {
        return showBackButton;
    }

    public void addStopwatchLap() {

        ArrayList<StopwatchLap> laps = getStopwatchLaps().getValue();

        if (laps != null) {
            StopwatchLap lap = new StopwatchLap();
            if (laps.size() > 0) {
                lap.setStartTime(laps.get(laps.size() - 1).getEndTime());
            }
            lap.setEndTime(elapsedTime.getValue());
            laps.add(lap);
        }
        stopwatchLaps.setValue(laps);
    }

    public LiveData<ArrayList<StopwatchLap>> getStopwatchLaps() {
        return stopwatchLaps;
    }

    public void resetLaps() {
        stopwatchLaps.setValue(new ArrayList<>());
        elapsedTime.setValue(new Time());
        stopwatchManager.resetLaps();
        stopwatchPaused.setValue(true);
    }

    public LiveData<Boolean> getStopwatchPaused() {
        return stopwatchPaused;
    }

    public void startStopwatch() {
        stopwatchManager.startStopwatch();
        stopwatchPaused.setValue(false);
    }

    public void pauseStopwatch() {
        stopwatchManager.pauseStopwatch();
        stopwatchPaused.setValue(true);

    }
}
