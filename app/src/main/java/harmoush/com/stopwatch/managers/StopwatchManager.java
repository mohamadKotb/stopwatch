package harmoush.com.stopwatch.managers;

import harmoush.com.stopwatch.models.Time;

public class StopwatchManager {

    private boolean pauseTimer = false;
    private ElapsedTimeUpdatedListener elapsedTimeUpdatedListener;

    private Time elapsedTime;

    public StopwatchManager(ElapsedTimeUpdatedListener listener) {
        elapsedTimeUpdatedListener = listener;
        resetLaps();
    }

    public void startStopwatch() {
        pauseTimer = false;
        startTimer();
    }

    private void startTimer() {

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!pauseTimer) {
                        Thread.sleep(1000);
                        if (!pauseTimer) {

                            if (elapsedTime.getSeconds() >= 60) {
                                elapsedTime.setSeconds(0);
                                elapsedTime.setMinutes(elapsedTime.getMinutes() + 1);
                            } else {
                                elapsedTime.setSeconds(elapsedTime.getSeconds() + 1);
                            }

                            elapsedTimeUpdatedListener.onElapsedTimeUpdate(elapsedTime);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public void pauseStopwatch() {
        pauseTimer = true;
    }

    public void resetLaps() {
        elapsedTime = new Time();
    }

    public interface ElapsedTimeUpdatedListener {
        void onElapsedTimeUpdate(Time time);
    }
}
